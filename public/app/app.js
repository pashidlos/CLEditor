angular.module('app', [
    'ngResource',
    'ngRoute',
    'ui.tree',
    'ui.tree-filter',
    'ngAnimate',
    'ngAria',
    'ngMaterial',

    'xeditable']);

angular.module('app').config(function ($routeProvider, $locationProvider, uiTreeFilterSettingsProvider) {
    var routeRoleChecks = {
        admin: {
            auth: function(mvAuth) {
                mvAuth.authorizeCurrentUserForRoute('admin');
            }
        },
        user: {
            auth: function (mvAuth) {
                mvAuth.authorizeAuthenticatedUserForRoute();
            }
        }
    };

    uiTreeFilterSettingsProvider.descendantCollection = 'features';

    $locationProvider.html5Mode(true);
    $routeProvider
        .when('/', {templateUrl: '/partials/main/main', controller: 'mvMainCtrl'})
        .when('/signup', {templateUrl: '/partials/account/signup', controller: 'mvSignupCtrl'})
        .when('/profile', {templateUrl: '/partials/account/profile',
            controller: 'mvProfileCtrl', resolve: routeRoleChecks.user})
        .when('/admin/users', {templateUrl: '/partials/admin/user-list',
            controller: 'mvUserListCtrl', resolve: routeRoleChecks.admin})
        .when('/tree', {templateUrl: '/partials/tree/tree', controller: 'mvTreeCtrl'})
});

angular.module('app').run(function($rootScope, $location, editableOptions){
    editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
    $rootScope.$on('$routeChangeError', function(evt, current, previous, rejection){
        if (rejection === 'Not authorized') {
            $location.path('');
        }
    })
});
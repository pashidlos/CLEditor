angular.module('app').controller('mvMainCtrl', function ($scope) {
    $scope.courses = [
        {name: 'C# for beginers', featured: true, published: new Date(2017, 1)},
        {name: 'C# for juniors', featured: true, published: new Date(2017, 1)},
        {name: 'C# for seniors', featured: true, published: new Date(2017, 1)},
        {name: 'Java for beginers', featured: false, published: new Date(2017, 1)},
        {name: 'Java for juniors', featured: false, published: new Date(2017, 3)},
        {name: 'Java for seniors', featured: false, published: new Date(2017, 2)}
    ]
});
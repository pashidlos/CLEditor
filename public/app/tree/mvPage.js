angular.module('app').factory('mvPage', function($resource) {
    var PageResource = $resource('/api/pages/:id', {_id: '@id'},{
        update: {method:'PUT', isArray:false}
    });
    return PageResource;
});
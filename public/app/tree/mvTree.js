angular.module('app').factory('mvTree', function ($http, $q, mvPage) {
    return {
        updatePage: function (page) {
            var dfd = $q.defer();

            page.$update().then(function (response) {
                dfd.resolve(response);
            }, function (response) {
                dfd.reject(response.data.reason)
            });
            return dfd.promise;
        },

        addPage: function (newPageData) {
            var newPage = new mvPage(newPageData);
            var dfd = $q.defer();

            newPage.$save().then(function (response) {
                dfd.resolve(response);
            }, function (response) {
                dfd.reject(response.data.reason)
            });
            return dfd.promise;
        },

        deletePage: function (page) {
            var dfd = $q.defer();

            page.$delete(page).then(function () {
                dfd.resolve();
            }, function (response) {
                dfd.reject(response.data.reason)
            });
            return dfd.promise;
        },
    }
});
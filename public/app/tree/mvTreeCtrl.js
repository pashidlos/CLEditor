angular.module('app').controller('mvTreeCtrl', function ($filter, $scope, mvPage, mvTree, mvNotifier) {
    $scope.allowDragAndDrop = false;
    $scope.allowRemove = false;

    $scope.treeFilter = $filter('uiTreeFilter');
    $scope.availableFields = ['title'];
    $scope.supportedFields = ['title'];

    $scope.toggleSupport = function (propertyName) {
        return $scope.supportedFields.indexOf(propertyName) > -1 ?
            $scope.supportedFields.splice($scope.supportedFields.indexOf(propertyName), 1) :
            $scope.supportedFields.push(propertyName);
    };

    $scope.newPage = function (data) {
        var newPageData = {
            title: "Page" + '.' + (data.length + 1),
            isTest: false,
            features: []
        };

        mvTree.addPage(newPageData).then(function (newPage) {
            mvNotifier.notify('Page has been added');
            $scope.data.push(newPage);
        }, function (reason) {
            mvNotifier.error(reason)
        });
    };

    $scope.deletePage = function (data, index) {
        mvTree.deletePage(data).then(function () {
            mvNotifier.notify('Page has been removed');
            $scope.data.splice(index, 1);
        }, function (reason) {
            mvNotifier.error(reason)
        });
    };

    $scope.newFeature = function (page, element) {
        var newFeature = {
            title: "Feature" + '.' + (element.features.length + 1),
            isTest: false,
            features: []
        };

        element.features.push(newFeature);

        mvTree.updatePage(page).then(function (response) {
            mvNotifier.notify('Feature has been added');
        }, function (reason) {
            mvNotifier.error(reason)
        });
    };

    $scope.newTest = function (page, element) {
        var newTest = {
            title: "Test" + '.' + (element.features.length + 1),
            isTest: true,
            features: []
        };

        element.features.push(newTest);

        mvTree.updatePage(page).then(function (response) {
            mvNotifier.notify('Feature has been added');
        }, function (reason) {
            mvNotifier.error(reason)
        });
    };

    $scope.updatePage = function (item) {
        mvTree.updatePage(item).then(function () {
            mvNotifier.notify('Item has been updated');
        }, function (reason) {
            mvNotifier.error(reason)
        });
    }

    $scope.collapseAll = function () {
        $scope.$broadcast('angular-ui-tree:collapse-all');
    };

    $scope.expandAll = function () {
        $scope.$broadcast('angular-ui-tree:expand-all');
    };

    $scope.changeDragAndDropStatus = function () {
        $scope.allowDragAndDrop = $scope.allowDragAndDrop === false;
    };

    $scope.changeRemoveStatus = function () {
        $scope.allowRemove = $scope.allowRemove === false;
    };

    $scope.showStatus = function(node) {
        var status = node.status;
        var selected = $filter('filter')($scope.statuses, {value: status});
        if (status && selected.length) {
            return selected[0].text;
        } else {
            return 'Not set';
        }
    };

    var getStatusesStatistic = function (node) {
        for (var i = 0; i < node.items.length; i++) {
            getStatusesStatistic(node.items[i]);
        }
        if (node.items.length === 0){
            var selected = $filter('filter')($scope.statuses, {value: node.status});
            if (node.status && selected.length) {
                selected[0].quantity++;
            }
        }
    };

    $scope.showStatusCount = function () {
        $scope.statuses.forEach(function (status) {
            status.quantity = 0;
        });
        $scope.data.forEach(function (node){
            getStatusesStatistic(node);
        });
    };

    $scope.statuses = [
        {value: 1, text: 'Not yet automated', quantity: 0},
        {value: 2, text: 'Automated', quantity: 0},
        {value: 3, text: 'Manual check', quantity: 0},
        {value: 4, text: 'Blocked', quantity: 0},
        {value: 5, text: 'Not implemented feature', quantity: 0}
    ];

    $scope.data = mvPage.query();
});
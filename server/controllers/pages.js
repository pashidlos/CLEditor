var Page = require('mongoose').model('Page');


exports.getPages = function (req, res) {

    Page.find({}).exec(function (err, collection) {
        res.send(collection);
    })
};

exports.updatePage = function (req, res, next) {
    Page.findOneAndUpdate({_id:req.body._id}, req.body, function (err) {
        if(err) {
            res.status(400);
            return res.send({reason: err.toString()});
        }
        res.send(req.body);
    })
};

exports.addPage = function (req, res, next) {
    var pageData = req.body;
    Page.create(pageData, function (err, user) {
        if (err) {
            res.status(400);
            return res.send({reason: err.toString()});
        }
        res.send(pageData);
    })
};

exports.deletePage = function (req, res, next) {
    Page.findOneAndRemove({_id:req.query._id}, function (err) {
        if(err) {
            res.status(400);
            return res.send({reason: err.toString()});
        }
        res.send(req.body);
    })
};

var mongoose = require('mongoose');


var pageSchema = mongoose.Schema({
    title: {
        type:String,
        required: '{PATH} is required!'},
    isTest: Boolean,
    status: Number,
});

pageSchema.add({
    features: [pageSchema]
});

pageSchema.methods = {

};

var Page = mongoose.model('Page', pageSchema);

function createDefaultPages() {
    Page.find({}).exec(function (err, collection) {
        if (collection.length === 0) {
            Page.create({
                title: 'Guest list',
                features: [
                    {
                        title: "Filter",
                        isTest: false,
                        features: [
                            {
                                title: "Usual filter",
                                isTest: false,
                                features: [
                                    {
                                        title: "All customers",
                                        features: [],
                                        isTest: true,
                                        status: 2
                                    },
                                    {
                                        title: "Business Guests",
                                        features: [],
                                        isTest: true,
                                        status: 2
                                    },
                                    {
                                        title: "Inquiries",
                                        features: [],
                                        isTest: true,
                                        status: 2
                                    },
                                    {
                                        title: "New guests",
                                        features: [],
                                        isTest: true,
                                        status: 2
                                    },
                                    {
                                        title: "Possible Duplicates",
                                        features: [],
                                        isTest: true,
                                        status: 2
                                    },
                                    {
                                        title: "Repeat Guests",
                                        features: [],
                                        isTest: true,
                                        status: 2
                                    },
                                ]
                            },
                            {
                                title: "Custom filters",
                                isTest: false,
                                features: [
                                    {
                                        title: "Can use",
                                        features: [],
                                        isTest: true,
                                        status: 2
                                    },
                                    {
                                        title: "Can go to filter creation",
                                        features: [],
                                        isTest: true,
                                        status: 2
                                    },
                                    {
                                        title: "Can go to edit",
                                        features: [],
                                        isTest: true,
                                        status: 2
                                    },
                                    {
                                        title: "Can delete",
                                        features: [],
                                        isTest: true,
                                        status: 2
                                    },
                                ],
                            }
                        ]
                    },
                    {
                        title: "Search",
                        isTest: false,
                        features: [
                            {
                                title: "First name",
                                features: [],
                                isTest: true,
                                status: 2
                            },
                            {
                                title: "Last name",
                                features: [],
                                isTest: true,
                                status: 2
                            },
                            {
                                title: "Company name",
                                features: [],
                                isTest: true,
                                status: 2
                            },
                            {
                                title: "AddrStreet1",
                                features: [],
                                isTest: true,
                                status: 2
                            },
                            {
                                title: "AddrState",
                                features: [],
                                isTest: true,
                                status: 2
                            },
                            {
                                title: "AddrCity",
                                features: [],
                                isTest: true,
                                status: 2
                            },
                            {
                                title: "Email",
                                features: [],
                                isTest: true,
                                status: 2
                            }

                        ]
                    },
                ]
            });
            Page.create({
                title: 'Page1',
                features: [
                    {
                        title: "Test 1",
                        isTest: true,
                        features: [],
                        status: 2
                    },
                    {
                        title: "Test 2",
                        features: [],
                        isTest: true,
                        status: 2
                    },
                    {
                        title: "Feature 1",
                        isTest: false,
                        features: [
                            {
                                title: "Feature 1.1",
                                isTest: false,
                                features: [
                                    {
                                        title: "Test 1.1.1",
                                        features: [],
                                        isTest: true,
                                        status: 2
                                    },
                                    {
                                        title: "Test 1.1.2",
                                        features: [],
                                        isTest: true,
                                        status: 2
                                    },
                                    {
                                        title: "Feature 1.1.1",
                                        isTest: false,
                                        features: [
                                            {
                                                title: "Test 1.1.1.1",
                                                features: [],
                                                isTest: true,
                                                status: 2
                                            },
                                            {
                                                title: "Feature 1.1.1.1",
                                                isTest: false,
                                                features: [
                                                    {
                                                        title: "Test 1.1.1.1.1",
                                                        features: [],
                                                        isTest: true,
                                                        status: 2
                                                    },
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                title: "Test 1.1",
                                features: [],
                                isTest: true,
                                status: 2
                            },
                            {
                                title: "Test 1.2",
                                features: [],
                                isTest: true,
                                status: 2
                            }

                        ]
                    },
                    {
                        title: "Feature 2",
                        isTest: false,
                        features: [
                            {
                                title: "Test 2.1",
                                features: [],
                                isTest: true,
                                status: 2
                            },
                            {
                                title: "Test 2.2",
                                features: [],
                                isTest: true,
                                status: 2
                            }

                        ]
                    },
                ]
            });
            Page.create({
                title: 'Page2',
                features: [],
            });
            Page.create({
                title: 'Page3',
                features: [],
            });
            Page.create({
                title: 'Page4',
                features: [],
            });
        }
    })
}

exports.createDefaultPages = createDefaultPages;
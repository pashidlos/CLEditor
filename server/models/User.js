var mongoose = require('mongoose'),
    encryption = require('../utilities/encryption');


var userSchema = mongoose.Schema({
    firstName: {
        type:String,
        required: '{PATH} is required!'},
    lastName: {
        type:String,
        required: '{PATH} is required!'},
    username: {
        type:String,
        required: '{PATH} is required!',
        unique:true
    },
    salt: {
        type: String,
        required: '{PATH} is required!'
    },
    hashed_pwd: {
        type:String,
        required: '{PATH} is required!'},
    roles: [String]
});

userSchema.methods = {
    authenticate: function(password){
        return encryption.hashPwd(this.salt, password) === this.hashed_pwd;
    },
    hasRole: function (role) {
        return this.roles.indexOf(role) > -1;
    }
};

var User = mongoose.model('User', userSchema);

function createDefaultUsers() {
    User.find({}).exec(function (err, collection) {
        if (collection.length === 0) {
            var salt, hash;
            salt = encryption.createSalt();
            hash = encryption.hashPwd(salt,'joe');
            User.create({firstName: 'Joe', lastName:'Bert', username:'joe', salt: salt, hashed_pwd: hash, roles: ['admin']});
            salt = encryption.createSalt();
            hash = encryption.hashPwd(salt,'joe1');
            User.create({firstName: 'Joe1', lastName:'Bert1', username:'joe1', salt: salt, hashed_pwd: hash, roles: []});
            salt = encryption.createSalt();
            hash = encryption.hashPwd(salt,'joe2');
            User.create({firstName: 'Joe2', lastName:'Bert2', username:'joe2', salt: salt, hashed_pwd: hash});
        }
    })
}

exports.createDefaultUsers = createDefaultUsers;
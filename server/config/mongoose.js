var mongoose = require('mongoose'),
    User = require('../models/User'),
    Page = require('../models/Page');

module.exports = function (config) {
    mongoose.connect(config.db);

    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'DB connection error...'));
    db.once('open', function calback() {
        console.log('Connected to DB');
    });

    User.createDefaultUsers();
    Page.createDefaultPages();
};
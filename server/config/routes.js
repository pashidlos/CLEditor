var auth = require('./auth'),
    users = require('../controllers/users'),
    pages = require('../controllers/pages'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Page = mongoose.model('Page');

module.exports = function (app) {

    app.get('/api/users', auth.requiresRole('admin'), users.getUsers);
    app.post('/api/users', users.createUser);
    app.put('/api/users', users.updateUser);

    app.get('/api/pages', pages.getPages);
    app.put('/api/pages', pages.updatePage);
    app.post('/api/pages', pages.addPage);
    app.delete('/api/pages', pages.deletePage);

    app.get('/partials/*', function (req, res) {
        res.render('../../public/app/' + req.params[0]);
    });

    app.post('/login', auth.authenticate);

    app.post('/logout', function (req, res) {
        req.logout();
        res.end();
    });

    app.get('*', function (req, res) {
        res.render('index', {
            bootstrappedUser: req.user
        });
    });
};
describe('mvUser', function(){
    beforeEach(module('app'));

    describe('isAdmin', function() {
        it('Should return false if user is not an admin', inject(function (mvUser){
            var user = new mvUser();
            user.roles = ['not admin'];

            expect(user.isAdmin()).to.be.falsey;
        }));

        it('Should return true if user is an admin', inject(function (mvUser){
            var user = new mvUser();
            user.roles = ['admin'];

            expect(user.isAdmin()).to.be.true;
        }));
    })
});
var express = require('express');

var env = process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var app = express();
var config = require('./server/config/config')[env];

//Express
require('./server/config/express')(app, config);

//DB
require('./server/config/mongoose')(config);

//Login
require('./server/config/passport')();

//Route
require('./server/config/routes')(app);

app.listen(config.port);
console.log('Listening on port ' + config.port + '...');